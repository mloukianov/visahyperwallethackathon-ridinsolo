/*
 * Copyright (C) 2008-2017, Nine Line Labs LLC
 *
 * The program(s) herein may be used and/or copied only with
 * the written permission of Nine Line Labs LLC or in accordance with
 * the terms and conditions stipulated in the agreement/contract
 * under which the program(s) have been supplied.
 *
 * $Id: $
 *
 * Date Author Changes
 * Mar 14, 2017 maxloukianov Created
 *
 */
package json;

/**
 * @author maxloukianov
 *
 */
public class HWResponse {

	public String token;
	
	public String pmtToken;
}
