package controllers;

import play.*;
import play.libs.Codec;
import play.mvc.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.mchange.util.Base64Encoder;

import json.HWResponse;
import json.HWResponse2;
import models.*;

public class Application extends Controller {

    public static void index(String id) {
    	
    	String decodedZip = new String(Codec.decodeBASE64(id));
    	
    	if (decodedZip.equals("78701")) {
    		
    		String location = "disaster";
    		
    		render(location);
    	} {
    		String location = "donate";
    		
    		render(location);
    	}
    }
    
    
    public static void doSubmit(String email, String accountType, String accountNo, String expDate, String firstname, String lastname, String password) throws URISyntaxException, ClientProtocolException, IOException {
    	
    	Logger.info("Submission: %s, %s, %s, %s, %s, %s, %s", email, accountType, accountNo, firstname, lastname, expDate, password);
    	
    	Integer amount = 50;
    	
    	if (HWFundsRecipient.count("byEmail", email) == 0) {
    	
	    	HWFundsRecipient recipient = new HWFundsRecipient();
	    	recipient.email = email;
	    	recipient.accountType = accountType;
	    	recipient.accountNo = accountNo;
	    	recipient.expDate = expDate;
	    	recipient.firstname = firstname;
	    	recipient.lastname = lastname;
	    	recipient.password = password;
	    	
	    	recipient.save();
	    	
			HWResponse hwResponse = doUserCreation(email, firstname, lastname, recipient);
			
			recipient.token = hwResponse.token;
			recipient.save();
			
			String transferToken = doCreateTransferMethod(recipient, accountNo);
			
			String programToken = "prg-f49e4c92-975b-41f4-9b70-922ed336d935";
			
			HWPayment payment = new HWPayment();
			payment.amount = amount;
			payment.recipient = recipient;
			payment.save();
			
			Application.doTransferMoney(recipient, payment, accountNo, amount, hwResponse.token, programToken);
		
    	} else {
    		
    		HWFundsRecipient recipient = HWFundsRecipient.find("byEmail", email).first();
    		
    		
    	}

		/*
{
  "clientUserId": "C301245",
  "profileType" :"INDIVIDUAL",
  "firstName": "John",
  "lastName": "Deer",
  "email": "jdeer@hyperwallet.com",
  "addressLine1": "123 Home Road",
  "city": "San Francisco",
  "stateProvince": "CA",
  "country": "US",
  "postalCode": "90012",
  "programToken": "prg-eb305d54-00b4-432b-eac2-ab6b9e123409"
}		 */
    	
    	render(firstname, lastname, amount, email, accountNo, accountType);
    }

    private static void doTransferMoney(HWFundsRecipient recipient, HWPayment payment, String accountNo, Integer amount, String transferToken, String programToken) 
    		throws URISyntaxException, UnsupportedEncodingException, IOException, ClientProtocolException {

		URI uri = new URIBuilder()
		        .setScheme("https")
		        .setHost("api.sandbox.hyperwallet.com")
		        .setPath("/rest/v3/payments")
		        .build();
		
		HttpPost httppost = new HttpPost(uri);
		
		String authorizationString = "Basic " + Codec.encodeBASE64("restapiuser@6653571619:98Hyperwallet@");
		
		httppost.addHeader("Content-Type", "application/json");
		httppost.addHeader("Accept", "application/json");
		httppost.setHeader("Authorization", authorizationString);
		
		System.out.println(httppost.getURI());
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		
		StringBuffer bufferData = new StringBuffer();
		
		bufferData.append("{").
			  append("\"clientPaymentId\" :\""+ payment.id + "\",").
			  append("\"amount\": \"" + amount + "\",").
			  append("\"currency\": \"USD\",").
			  append("\"description\": \"Relief package for " + recipient.email + "\",").
			  append("\"purpose\": \"OTHER\",").
			  append("\"destinationToken\": \"").append(transferToken).append("\",").
			  append("\"programToken\": \"").append(programToken).append("\"").
			append("}");
		
		Logger.info(bufferData.toString());
		
		StringEntity entity = new StringEntity(bufferData.toString());
		
		httppost.setEntity(entity);
		httppost.setHeader("Content-type", "application/json");
		
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		
		Logger.info("About to send the POST request to SSI");
		
		HttpResponse httpresponse = httpClient.execute(httppost);
		
		HttpEntity responseentity = httpresponse.getEntity();
		String responseStr = EntityUtils.toString(responseentity, "UTF-8");
		
		Logger.info("Response from HW: %s", responseStr);
		
		/*
		HWResponse2 hwResponse = new Gson().fromJson(responseStr, HWResponse2.class);
		
		Logger.info("Response token is: %s", hwResponse.token);
		
		return hwResponse.token;
		*/
	}
    
    private static String doCreateTransferMethod(HWFundsRecipient recipient, String accountNo) 
    		throws URISyntaxException, UnsupportedEncodingException, IOException, ClientProtocolException {
    	
		
		URI uri = new URIBuilder()
		        .setScheme("https")
		        .setHost("api.sandbox.hyperwallet.com")
		        .setPath("/rest/v3/users/" + recipient.token + "/bank-accounts")
		        .build();
		
		HttpPost httppost = new HttpPost(uri);
		
		String authorizationString = "Basic " + Codec.encodeBASE64("restapiuser@6653571619:98Hyperwallet@");
		
		httppost.addHeader("Content-Type", "application/json");
		httppost.addHeader("Accept", "application/json");
		httppost.setHeader("Authorization", authorizationString);
		
		System.out.println(httppost.getURI());
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		
		StringBuffer bufferData = new StringBuffer();
		
		bufferData.append("{").
			  append("\"type\" :\"BANK_ACCOUNT\",").
			  append("\"transferMethodCountry\": \"US\",").
			  append("\"transferMethodCurrency\": \"USD\",").
			  append("\"branchId\": \"021000021\",").
			  append("\"bankAccountId\": \"").append(accountNo).append("\",").
			  append("\"bankAccountPurpose\": \"SAVINGS\",").
			  append("\"bankAccountRelationship\": \"SELF\"").
			append("}");
		
		Logger.info(bufferData.toString());
		
		StringEntity entity = new StringEntity(bufferData.toString());
		
		httppost.setEntity(entity);
		httppost.setHeader("Content-type", "application/json");
		
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		
		Logger.info("About to send the POST request to SSI");
		
		HttpResponse httpresponse = httpClient.execute(httppost);
		
		HttpEntity responseentity = httpresponse.getEntity();
		String responseStr = EntityUtils.toString(responseentity, "UTF-8");
		
		Logger.info("Response from HW: %s", responseStr);
		
		HWResponse2 hwResponse = new Gson().fromJson(responseStr, HWResponse2.class);
		
		Logger.info("Response token is: %s", hwResponse.token);
		
		return hwResponse.token;
    }

	/**
	 * @param email
	 * @param fullname
	 * @param recipient
	 * @return
	 * @throws URISyntaxException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	private static HWResponse doUserCreation(String email, String firstname, String lastname, HWFundsRecipient recipient)
			throws URISyntaxException, UnsupportedEncodingException, IOException, ClientProtocolException {
		
		URI uri = new URIBuilder()
		        .setScheme("https")
		        .setHost("api.sandbox.hyperwallet.com")
		        .setPath("/rest/v3/users")
		        .build();
		
		HttpPost httppost = new HttpPost(uri);
		
		String authorizationString = "Basic " + Codec.encodeBASE64("restapiuser@6653571619:98Hyperwallet@");
		
		httppost.addHeader("Content-Type", "application/json");
		httppost.addHeader("Accept", "application/json");
		httppost.setHeader("Authorization", authorizationString);
		
		System.out.println(httppost.getURI());
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		
		StringBuffer bufferData = new StringBuffer();
		
		bufferData.append("{").
			  append("\"clientUserId\": \"").append(recipient.id + "").append("\",").
			  append("\"profileType\" :\"INDIVIDUAL\",").
			  append("\"firstName\": \"" + firstname + "\",").
			  append("\"lastName\": \"").append(lastname).append("\",").
			  append("\"email\": \"").append(email).append("\",").
			  append("\"addressLine1\": \"123 Home Road\",").
			  append("\"city\": \"San Francisco\",").
			  append("\"stateProvince\": \"CA\",").
			  append("\"country\": \"US\",").
			  append("\"postalCode\": \"90012\",").
			  append("\"programToken\": \"prg-f49e4c92-975b-41f4-9b70-922ed336d935\"").
			append("}");
		
		StringEntity entity = new StringEntity(bufferData.toString());
		
		httppost.setEntity(entity);
		httppost.setHeader("Content-type", "application/json");
		
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		
		Logger.info("About to send the POST request to SSI");
		
		HttpResponse httpresponse = httpClient.execute(httppost);
		
		HttpEntity responseentity = httpresponse.getEntity();
		String responseStr = EntityUtils.toString(responseentity, "UTF-8");
		
		Logger.info("Response from HW: %s", responseStr);
		
		HWResponse hwResponse = new Gson().fromJson(responseStr, HWResponse.class);
		
		Logger.info("Response token is: %s", hwResponse.token);
		return hwResponse;
	}
	
	
	
}